# automated_k8s_deployment_ansible_vagrant

## Status
Working on Debian-11 and Ubuntu-22.04 as latest versions. Newer versions
are not working at this moment.

## What is
Ansible playbook for automated deployment of a kubernetes cluster for Debian
 and Ubuntu with minimum necessary components to be ready to work.
Uses kubeadm to bootstrap the cluster.
There is also a Vagrant file to deploy the cluster in VMs on your laptops.
The default Vagrantfile just deploys 1 VM that works as kubernetes control plane.

## How to deploy cluster with ansible
You can run various playbooks available, just add your machine into the hosts file.
You need to have SSH access to that machine. 

Then do:
```
ansible-playbook playbooks/deploy_k8s_control_plane.yml
```
There are various playbooks available, for both control-plane deployments, 
worker nodes, debug etc...

## How to use the test environment on your local machine
If you want to run a test VM  on your machine just use the default configs and 
run choosing the OS you want:
```
vagrant up $machine
```
$machine must be "debian-cp" or "ubuntu-cp" depending on what you want.

You can ssh the machine with:
```
vagrant ssh
```
If you want to use kubernetes's kubectl, just become root and run it.

```
kubectl get nodes
```

To destroy the machine:
```
vagrant destroy
```

Note: To use the ansible playbooks on your machines, you must add your machine
 in the hosts file with the correct IP address and control_plane_IP variable.

## vagrant setup troubleshooting

You may have to install disk size plugin.
```
vagrant plugin install vagrant-disksize
```
